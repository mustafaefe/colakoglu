const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const prefix = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');

gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: '.'
        },
    })
});

gulp.task('sass', function(){
    return gulp.src('assets/sass/*.scss')
        .pipe(sass())
        .pipe(sourcemaps.init())
        .pipe(prefix())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('assets/css'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('watch', ['browserSync', 'sass'], function(){
    gulp.watch('assets/sass/*.scss', ['sass']);
    gulp.watch('*.html', browserSync.reload);
    gulp.watch('ik/*.html', browserSync.reload);
    gulp.watch('sosyal-sorumluluk/*.html', browserSync.reload);
    gulp.watch('*.js', browserSync.reload);
});

gulp.task('default', ['sass', 'watch']);